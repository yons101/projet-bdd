<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Support\Str;
use Illuminate\Http\Request;

class CategoryManagerController extends Controller
{
    //page admin
    //route GET /categorymanager
    public function index()
    {

        //selectioner tous les categories
        $categories = Category::all();
        //SELECT * FROM categories

        //afficher lview li kayna fi /resources/views/categorymanager/index.blade.php 
        //osift lya m3aha les variables 'categories'
        return view('categorymanager.index', compact('categories'));
    }



    //route GET /categorymanager/create
    public function create()
    {
        //afficher lview li kayna fi /resources/views/categorymanager/create.blade.php 
        return view('categorymanager.create');
    }



    //route POST /categorymanager
    public function store(Request $request)
    {

        //creer la category

        //inserer une categorie
        Category::create([
            'title' => $request->title,
            'slug' => Str::replaceArray(' ', ['-'], $request->title),
            'description' => $request->description,
        ]);
        /* 
        INSERT INTO categories (title, slug, description)
        VALUES (Category A, category-a, Category A Description)
        */

        //redirect la route 'GET /categorymanager', m3a wahd lmessage temporaire smito 'success'
        return redirect()->route('categorymanager.index')
            ->with('success', 'Category has been added successfully.');
    }



    //route GET /categorymanager/{id}
    public function edit($id)
    {
        //jib l category li ID = $id
        //sinon firstOrFail = 404
        $category = Category::where('id', $id)->firstOrFail();

        //afficher lview li kayna fi /resources/views/categorymanager/edit.blade.php 
        //osift lya m3aha les variables 'category'
        return view('categorymanager.edit', compact('category'));
    }

    //route PUT /productmanager/{id}
    public function update(Request $request, $id)
    {
        $category = Category::where('id', $id)->firstOrFail();

        //mise a jour d'une categorie
        $category->update(
            [
                'title' => $request->title,
                'slug' => Str::replaceArray(' ', ['-'], $request->title) .  '-'  . time(),
                'description' => $request->description,
            ]
        );
        /* 
        UPDATE categories
        SET
            title = "Category A",
            slug = "category-a",
            description = "Category A Description",
        WHERE id = 1
        */

        //redirect la route 'GET /categorymanager', m3a wahd lmessage temporaire smito 'success'
        return redirect()->route('categorymanager.index')
            ->with('success', 'Category has been updated successfully.');
    }

    //route DELETE /categorymanager/{id}
    public function destroy($id)
    {
        //jib l category li ID = $id
        //sinon firstOrFail = 404
        //supprimer une categorie
        Category::destroy($id);
        /* 
        DELETE FROM categories
        WHERE id = 1
        */
        //redirect la route 'GET /categorymanager', m3a wahd lmessage temporaire smito 'success'
        return redirect()->route('categorymanager.index')
            ->with('success', 'category has been deleted successfully.');
    }
}
