<?php

namespace App\Http\Controllers;

use App\Cart;
use App\User;
use App\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{

    //sauf l' users li mconectyin 
    //li 3ndhom le droit ysta3mlo had les methodes li f CartController
    public function __construct()
    {
        $this->middleware('auth');
    }
    /* 
        SELECT 
            products.id,
            products.title,
            products.slug,
            products.price,
            products.image,
            carts.quantity,
            (products.price * carts.quantity)
        FROM users
        INNER JOIN carts
        ON users.id = carts.user_id
        INNER JOIN products
        ON products.id = carts.product_id
        WHERE users.id = 1
        */
    //route GET /cart
    public function index()
    {
        //obtenir tous les produits qui sont dans le panier
        $products = Auth::user()->cart->products->groupBy('id');

        // dd($products);
        $info['totalprice'] = Auth::user()->cart->products()->sum('price');
        $info['quantity'] = Auth::user()->cart->products()->count();
        $info['lastId'] = 0;

        //obtenir la quantité total 
        //obtenir le prix total


        //afficher la page panier avec les variables 'products', 'info'
        return view('cart', compact(['products', 'info']));
    }



    //route POST /cart
    public function store(Request $request)
    {

        /* 
        SELECT product_id 
        FROM carts 
        WHERE product_id = $request->id
        */
        //obtenir l'id du produit 
        $product = Product::find($request->id);


        //ajouter au panier
        Auth::user()->cart->products()->attach($product);

        //redirection a la page panier
        return redirect()->route('cart.index')->with('success', 'Item Was Added To Your Cart');
    }

    //route PUT /cart/{id}
    public function update(Request $request, $id)
    {
        $product = Product::find($request->id);

        $qtyInDB = Auth::user()->cart->products()->where('product_id', $product->id)->count();
        $requestedQty = $request->qty;

        $x = $qtyInDB - $requestedQty;


        if ($x >= 1) {
            //ex : qtyInDB 4 and $requestedQty 2 => this query will run 2 times
            DB::delete('delete from cart_product where product_id = ? order by id desc limit ?', [$product->id, $x]);
        } else if ($x < 0) {

            //ex : nqtyInDB 2 and $requestedQty 4 => this query will run 2 times

            for ($i = 0; $i < abs($x); $i++) {
                Auth::user()->cart->products()->attach($product);
            }
        } else if ($requestedQty == 0) {
            Auth::user()->cart->products()->detach($product);
        }

        // dd($requestedQty);

        // Auth::user()->cart->products()->detach($product);
        return redirect()->back();
    }

    //route DELETE /cart/{id}

    public function destroy($rowId)
    {

        /* 
        DELETE FROM carts
        WHERE Auth::user()->id and product_id = $request->id;
        */
        //supprimer le produit avec id = $rowId
        $product = Product::find($rowId);
        Auth::user()->cart->products()->detach($product);

        //redirection a la page panier
        return redirect()
            ->route('cart.index')
            ->with('success', 'The Item Has Been Removed From Your Cart!');
    }

    //route POST /cart/empty
    public function empty()
    {
        //supprimer tous les produits depuis la panier

        /* 
        DELETE FROM carts
        WHERE Auth::user()->id;
        */
        Auth::user()->cart->products()->detach();
        //redirection a la page panier
        return redirect()
            ->route('cart.index')
            ->with('success', 'Your Cart Has Been Emptied!');
    }
}
