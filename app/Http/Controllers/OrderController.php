<?php

namespace App\Http\Controllers;

use App\Order;
use App\Shipping;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class OrderController extends Controller
{
    //route GET /orders
    public function index()
    {
        //jib tous les orders dyal lclient
        $orders = Auth::user()->orders()->paginate(5);

        //afficher lview li kayna fi /resources/views/orders/index.blade.php 
        //osift lya m3aha les variables 'orders'
        return view('orders.index', compact(['orders']));
    }

    //route POST /orders
    public function store(Request $request)
    {

        $order = Order::create([
            'user_id' => Auth::user()->id,
            'fullname' => $request->name,
            'address' => $request->address,
            'city' => $request->city,
            'province' => $request->province,
            'postal_code' => $request->zip,
            'phone' => $request->phone,
            'country' => $request->country
        ]);
        $products = Auth::user()->cart->products()->get()->groupBy('id');
        $orders = Auth::user()->orders()->paginate(5);


        foreach ($products as $product) {
            $order->products()->attach($product);
        }
        Auth::user()->cart->products()->detach();

        //afficher lview li kayna fi /resources/views/orders/index.blade.php 
        //osift lya m3aha les variables 'orders'
        return view('orders.index', compact(['orders']));
    }


    //route GET /orders/{id}
    public function show($id)
    {
        //jib tous les products li kayntamiw l nafs lorder
        //c-a-d afficher le contenu dyal lorder
        //pagination par 5
        $products = Order::findOrFail($id)->products()->latest()->paginate(5);

        //afficher lview li kayna fi /resources/views/orders/show.blade.php 
        //osift lya m3aha les variables 'products'
        return view('orders.show', compact(['products', 'id']));
    }
}
